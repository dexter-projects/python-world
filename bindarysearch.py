def binary_search(data, left, right, x):
    while left <= right:
        print(left, ' left', right, ' right')
        mid = left + (right - left) // 2

        if data[mid] == x:
            print(mid)
            return mid

        elif data[mid] < x:
            left = mid + 1

        else:
            right = mid - 1

    return -1


def sort_asc(data):
    for left, l in enumerate(data):
        for right, r in enumerate(data):
            right + 1
            if data[right] > data[left]:
                tmp = data[right]
                data[right] = data[left]
                data[left] = tmp

    return data


arr = [10, 20, 30, 190, 29, 19, 2, 78, 17, 3000]

v = 3000

arr = sort_asc(arr)
result = binary_search(arr, 0, len(arr) - 1, v)

if result != -1:
    print("Element is present at index % d" % result)
else:
    print("Element is not present in array")
