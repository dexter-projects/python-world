from pytube import YouTube
from pytube.cli import on_progress

url = 'https://www.youtube.com/watch?v=jOQ6f_3F2uw'

yt = YouTube(url, on_progress_callback=on_progress)

yt.streams.order_by('resolution').desc()

download_path = 'E:\Downloads\Youtube'

yt = yt.streams[0].download(download_path)
