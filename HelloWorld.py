def world(*world):
    for w in world:
        print("Hello ", w)


def do_math(num1, num2):
    return num1 + num2


world("World", "Python")

print(do_math(1, 2))